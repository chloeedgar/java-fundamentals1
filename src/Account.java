public class Account {
    private double balance;
    private String name;
    private static double interestRate; //%


    public Account(String name, double balance){
        this.name = name;
        this.balance = balance;
    }

    public Account() {
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addInterest(){
        //balance*=(1+interestRate/100);
    }

    @Override
    public String toString() {
        return "Account{" +
                "balance=" + balance +
                ", name='" + name + '\'' +
                '}';
    }
//    public static void main(String[] args) {
//        System.out.println("Balance: "+ balance);
//        addInterest();
//        System.out.println("Balance with added 10%: "+ balance);
//    }
}