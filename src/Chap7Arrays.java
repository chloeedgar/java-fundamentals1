

public class Chap7Arrays {
    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[5];

        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

        for (int i=0; i<5; i++) {
            Account newAccount = new Account();
            newAccount.setName(names[i]);
            newAccount.setBalance(amounts[i]);
            arrayOfAccounts[i]= newAccount;
            System.out.println(newAccount.getName() + " : £" + newAccount.getBalance());
            newAccount.addInterest();
            System.out.println(newAccount.getName() + " : £" + newAccount.getBalance());

        }


    }
}
