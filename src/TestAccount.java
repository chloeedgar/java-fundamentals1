public class TestAccount {
    public static void main(String[] args) {
        Account myAccount = new Account();
        myAccount.setBalance(1000);
        myAccount.setName("Chloe");

        System.out.println(myAccount.getName() + " has £" + myAccount.getBalance());
        myAccount.addInterest();
        System.out.println("After interest, " + myAccount.getName() + " has £" + myAccount.getBalance());
    }
}
