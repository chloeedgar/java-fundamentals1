
public class TestStrings {
    public static void main(String[] args) {
        String myString = "example.doc";
        System.out.println(myString);
        String myStringupdated = myString.replace("doc", "bak");
        System.out.println(myStringupdated);

        String string1 = "cat";
        String string2 = "dog";

        System.out.println("The strings are equal, true of false?: " + string1.equals(string2));

        // Comparing strings lexicographically
        // is string1>string2 = positive value, equal = 0...
        if (string1.compareTo(string2) < 0) {
            System.out.println(string2 + " is ahead of " + string1 + " in the alphabet");
        }
        System.out.println(string1.compareTo(string2));


//        // Number of times "ow" occurs in the phrase
//        String phrase = "the quick brown fox swallowed down the lazy chicken";
//        String letters = "ow";
//        System.out.println(phrase.charAt(0) + phrase.charAt(1));
//        int counter = 0;
//        for (int i=1; i<phrase.length(); i++) {
//            if (letters.equalsIgnoreCase(phrase.charAt(i-1)+ phrase.charAt(i))){
//                counter++;
//            }
//        }
//        System.out.println(counter);
//    }

        //4. Find the num of times "ow"
        String text = "the quick brown fox swallowed down the lazy chicken";

        int count = 0;

        for (int i=0; i<text.length()-2; i++) {
            if (text.substring(i, i+2).equals("ow")){
                count++;
            }
            System.out.println("The text ow appears %d times \n", count);
        }

        // 5. Check to see whether a given string is a palindrome (eg live not on evil")




    }
}
